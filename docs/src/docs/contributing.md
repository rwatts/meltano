# Contributing

## Prerequisites

::: warning
Before you move on, make sure you have Python `3.6`, Node `8.x`, Yarn `1.2` or newer installed.
:::

## Where to start?

We welcome contributions, idea submissions, and improvements. In fact we may already have open issues labeled [Accepting Merge Requests](https://gitlab.com/meltano/meltano/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Accepting%20Merge%20Requests) if you don't know where to start. Please see the contribution guidelines below for source code related contributions.

```bash
# Clone the Meltano repo
git clone git@gitlab.com:meltano/meltano.git

# Change directory into the Meltano project
cd meltano

# Optional, but it's best to have the latest pip
pip install --upgrade pip

# Optional, but it's best to have the latest setuptools
pip install --upgrade setuptools

# Optional, but it's recommended to create a virtual environment
# in order to minimize side effects from unknown environment variable
python -m venv ~/virtualenvs/meltano-development

# Activate your virtual environment
source ~/virtualenvs/meltano-development/bin/activate

# Install all the dependencies
pip install -r requirements.txt

# Install dev dependencies with the edit flag on to detect changes
# Note: you may have to escape the .`[dev]` argument on some shells, like zsh
pip install -e .[dev]

# Bundle the Meltano UI into the `meltano` package
make bundle
```

Meltano is now installed and available at `meltano`.

Head out to the [tutorials](/docs/tutorial.html) to create your first project.

### Meltano API Development

For all changes that do not involve working on Meltano UI (front-end) itself, run the following command:

```bash
# Starts both Meltano API and a production build of Meltano UI
FLASK_ENV=development meltano ui
```

The development build of the Meltano API should be available at [http://localhost:5000/]

:::warning Troubleshooting
If you run into `/bin/sh: yarn: command not found`, double check that you've got [the prerequisites](https://www.meltano.com/docs/contributing.html#prerequisites) installed.

On a OSX, this can be solved by running `brew install yarn`.
:::

### Meltano UI Development

In the event you are contributing to Meltano UI and want to work with all of the frontend tooling (i.e., hot module reloading, etc.), you will need to run the following commands:

```bash
# Navigate to a Meltano project that has already been initialized
# Start the Meltano API and a production build of Meltano UI that you can ignore
meltano ui

# Open a new terminal tab and go to your meltano directory
# Install frontend infrastructure at the root directory
yarn

# Change directory to webapp source code
cd src/webapp

# Install dependencies
yarn

# Start local development environment
yarn serve
```

The developement build of the Meltano UI will be available at [http://localhost:8080/]

### Meltano System Database

Meltano API and CLI are both supported by a database that is managed using Alembic migrations.

:::tip Note
[Alembic](https://alembic.sqlalchemy.org/en/latest/) is a full featured database migration working on top of SQLAlchemy.
:::

Migrations for the system database are located inside the `meltano.migrations` module.

To create a new migration, use the `alembic revision -m "message"` command, then edit the created file with the necessary database changes. Make sure to implement both `upgrade` and `downgrade`, so the migration is reversible, as this will be used in migration tests in the future.

Each migration should be isolated from the `meltano` module, so **don't import any model definition inside a migration**.

:::warning
Meltano doesn't currently support auto-generating migration from the models definition.
:::

To run the migrations, use `meltano upgrade` inside a Meltano project.

## Documentation

### Text

Follow the merge request and changelog portions of this contribution section for text-based documentation contributions.

### Images

When adding supporting visuals to documentation, adhere to:

- Use Chrome in "incognito mode" (we do this to have the same browser bar for consistency across screenshots)
- Screenshot the image at 16:9 aspect ratio with minimum 1280x720px
- Place `.png` screenshot in `src/docs/.vuepress/public/screenshots/` with a descriptive name

## Code style

### Tools

Meltano uses the below tools to enforce consistent code style. Explore the [repo](https://gitlab.com/meltano/meltano/tree/master) to learn of the specific rules and settings of each.

- [Black](https://github.com/ambv/black)
- [ESLint](https://eslint.org/docs/rules/)
- [ESLint Vue Plugin](https://github.com/vuejs/eslint-plugin-vue)
- [Prettier](https://prettier.io/)

You may use `make lint` to automatically lint all your code, or `make show_lint` if you only want to see what needs to change.

### Mantra

> A contributor should know the exact line-of-code to make a change based on convention

In the spirit of GitLab's "boring solutions" with the above tools and mantra, the frontend codebase is additionally sorted as follows:

- `import`s are alphabetical and subgrouped by _core_ -> _third-party_ -> _application_ with a return delineating. For example:

  ```js
  // core
  import Vue from 'vue'
  // third-party
  import lodash from 'lodash'
  // application
  import poller from '@/utils/poller'
  import utils from '@/utils/utils'
  ```

- object properties and methods are alphabetical where `Vuex` stores are the exception (`defaultState` -> `getters` -> `actions` -> `mutations`)

Over time we hope to automate the enforcement of the above sorting rules.

:::warning Troubleshooting
When testing your contributions you may need to ensure that your various `__pycache__` directories are removed. This helps ensure that you are running the code you expect to be running.
:::

## UI/UX

### Visual Hierarchy

#### Depth

The below level hierarchy defines the back to front depth sorting of UI elements. Use it as a mental model to inform your UI decisions.

- Level 1 - Primary navigation, sub-navigation, and signage - _Grey_
- Level 2 - Task container (traditionally the page metaphor) - _White-ish Grey_
- Level 3 - Primary task container(s) - _White w/Shadow_
- Level 4 - Dropdowns, dialogs, pop-overs, etc. - _White w/Shadow_
- Level 5 - Modals - _White w/Lightbox_
- Level 6 - Toasts - _White w/Shadow + Message Color_

#### Interactivity

Within each aforementioned depth level is an interactive color hierarchy that further organizes content while communicating an order of importance for interactive elements. This interactive color hierarchy subtly influences the user's attention and nudges their focus.

1. Primary - _`$interactive-primary`_
   - Core interactive elements (typically buttons) for achieving the primary task(s) in the UI
   - Fill - Most important
   - Stroke - Important
1. Secondary - _`$interactive-secondary`_
   - Supporting interactive elements (various controls) that assist the primary task(s) in the UI
   - Fill - Hover only
   - Stroke - Denotes the states of selected, active, and/or valid
     - Grey represents the opposites: unselected, inactive, and/or invalid
1. Tertiary - _Greyscale_
   - Typically white buttons and other useful controls that aren't core or are in support of the primary task(s) in the UI
1. Navigation - _`$interactive-navigation`_
   - Denotes navigation and sub-navigation interactive elements as distinct from primary and secondary task colors

After the primary, secondary, tertiary, or navigation decision is made, the button size decision is informed by:

1. Use the default button size
1. Use the `is-small` modifier if it is within a component that can have multiple instances

### Markup Hierarchy

There are three fundamental markup groups in the codebase. All three are technically VueJS single-file components but each have an intended use:

1. Views (top-level "pages" and "page containers" that map to parent routes)
2. Sub-views (nested "pages" of "page containers" that map to child routes)
3. Components (widgets that are potentially reusable across parent and child routes)

Here is a technical breakdown:

1. Views - navigation, signage, and sub-navigation
   - Use `<router-view-layout>` as root with only two children:
     1. `<div class="container view-header">`
        - Signage
        - Sub-navigation
     2. `<div class="container view-body">`
        - Can expand based on task real-estate requirements via `is-fluid` class addition
   - Reside in the `src/views` directory
2. Sub-views - tasks
   - Use `<section>` as root (naturally assumes a parent of `<div class="container view-body">`) with one type of child:
     - One or more `<div class="columns">` each with their needed `<div class="column">` variations
   - Reside in feature directory (ex. `src/components/analyze/AnalyzeModels`)
3. Components - task helpers
   - Use Vue component best practices
   - Reside in feature or generic directory (ex. `src/components/analyze/ResultTable` and `src/components/generic/Dropdown`)

## Merge Requests

:::tip Searching for something to work on?
Start off by looking at our [~"Accepting Merge Request"](https://gitlab.com/meltano/meltano/issues?label_name=Accepting+Merge+Requests) label.

Keep in mind that this is only a suggestion: all improvements are welcome.
:::

Meltano uses an approval workflow for all merge requests.

1. Create your merge request
1. Assign the merge request to any Meltano maintainer for a review cycle
1. Once the review is done the reviewer may approve the merge request or send it back for further iteration
1. Once approved, the merge request can be merged by any Meltano maintainer

### Reviews

A contributor can ask for a review on any merge request, without this merge request being done and/or ready to merge.

Asking for a review is asking for feedback on the implementation, not approval of the merge request. It is also the perfect time to familiarize yourself with the code base. If you don’t understand why a certain code path would solve a particular problem, that should be sent as feedback: it most probably means the code is cryptic/complex or that the problem is bigger than anticipated.

Merge conflicts, failing tests and/or missing checkboxes should not be used as ground for sending back a merge request without feedback, unless specified by the reviewer.

## Changelog

Meltano uses [changelog-cli](https://github.com/mc706/changelog-cli) to populate the CHANGELOG.md

### Script

Use `changelog (new|change|fix|breaks) MESSAGE` to describe your current work in progress.

```bash
$ changelog new "add an amazing feature"
$ git add CHANGELOG.md
```

Make sure to add CHANGELOG entries to your merge requests.

## Releases

### Schedule

Meltano releases weekly on Monday mornings, and you can find a list of upcoming changes in our [CHANGELOG under "Unreleased"](https://gitlab.com/meltano/meltano/blob/master/CHANGELOG.md#unreleased)

#### Rotation

| Release Date | Release Owner | Dogfooding Owner | Shadow      |
| ------------ | ------------- | ---------------- | ----------- |
| 2019-09-03   | Derek K.      | Derek K.         | --          |
| 2019-09-09   | Ben H.        | Ben H.           | Douwe M.    |
| 2019-09-16   | Yannis R.     | Yannis R.        | Douwe M.    |
| 2019-09-23   | Douwe M.      | Douwe M.         | Danielle M. |
| 2019-09-30   | Danielle M.   | Danielle M.      |             |
| 2019-10-07   | Micael B.     | Micael B.        |             |
| 2019-10-14   | Derek K.      | Derek K.         |             |

:::tip Can't make your scheduled release?
If you are unable to cover an assigned week, please find someone to cover for you and submit an MR to this page with the new owner.
:::

### Versioning

Meltano uses [semver](https://semver.org/) as its version number scheme.

### Prerequisites

Ensure you have the latest `master` branch locally before continuing.

```bash
git fetch origin
```

### Release Process

Meltano uses tags to create its artifacts. Pushing a new tag to the repository will publish it as docker images and a PyPI package.

1. Meltano has a number of dependencies for the release toolchain that are required when performing a release. If you haven't already, please navigate to your meltano install and run the following command to install all development dependencies:

   ```bash
   # activate your virtualenv
   source ./venv/bin/activate

   # pip install all the development dependencies
   pip install .[dev]
   ```

1. Execute the commands below:

   ```bash
   # create and checkout the `release-next` branch from `origin/master`
   git checkout -B release-next origin/master

   # view changelog (verify changes made match changes logged)
   changelog view

   # after the changelog has been validated, tag the release
   make release

   # ensure the tag once the tag has been created, check the version we just bumped to: e.g. `0.22.0` => `0.23.0`.
   git describe --tags --abbrev=0

   # push the tag upstream to trigger the release pipeline
   git push origin $(git describe --tags --abbrev=0)

   # push the release branch to merge the new version, then create a merge request
   git push origin release-next
   ```
   
:::tip Releasing a hotfix? 
You can use `make type=patch release` to force a patch release. This is useful when we need to release hotfixes.
:::

1. Create a merge request from `release-next` targeting `master` and make sure to check `delete the source branch when the changes are merged`.
1. Add the pipeline link (the one that does the actual deployment) to the merge request. Go to the commit's pipelines tab and select the one that has the **publish** stage.
1. When the **publish** pipeline succeeds, the release is publicly available.

## Dogfood Workflow

1. Check that Meltano does not exist on your machine

```bash
meltano --version
# command not found: meltano
```

2. Install Meltano on your machine using distributed version

```bash
pip install meltano
```

3. Check Meltano version matches latest release

```bash
meltano --version
```

4. Create a new Meltano project

```bash
meltano init dogfood-workflow
```

5. Change directory into your new project

```bash
cd dogfood-workflow
```

6. Start Meltano application

```
meltano ui
```

7. Assuming there are no conflicts on the port, you can now open your Meltano instance at http://localhost:5000.

8. When you land on the Pipelines page, install `tap-gitlab`

9. Fill out `Private Token` with your token

10. Fill out `Groups` with `meltano`

11. Install `target-postgres`

12. Fill out Postgres configurations

13. Select `Run` for Transforms and hit `Save`.

14. Verify `Pipeline Schedule` configurations

15. Update `Catch-up Date` with beginning of the month to minimize the data request

16. Click `Save`. Pipeline should automatically be running now

17. Visit `Orchestrate` link on the top nav

18. Airflow UI should appear automatically with a DAG prepopulated

19. Verify `Pipeline Schedule` is complete before moving onto next step

20. Go to `Analyze`

21. Go into `Connections` in order to update settings and click `Save`

22. Click `Analyze` next to `Gitlab stats per user`

23. Select "User Name" and "Total Issues Assigned" on the left

24. Click `Run` to the right of Query

25. Verify Chart appears

26. Save as new report with automatically populated name

27. Add to New Dashboard with automatically populated name

28. Check `Edit` functionality on existing Report

## Taps & Targets Workflow

### For existing taps/targets

We should be good citizen about these, and use the default workflow to contribute. Most of these are on GitHub so:

1. Fork (using Meltano organization)
1. Add a [webhook](https://docs.gitlab.com/ee/ci/triggers/#triggering-a-pipeline-from-a-webhook) to trigger the `meltano/meltano` pipeline.
1. Modify and submits PRs
1. If there is resistance, fork as our tap (2)

### For taps/targets we create

1. For tap development please use the [tap cookiecutter template](https://github.com/singer-io/singer-tap-template).
1. For target developement please use the [target cookiecutter template](https://github.com/singer-io/singer-target-template).
1. Use a separate repo (meltano/target|tap-x) in GitLab
   e.g. Snowflake: https://gitlab.com/meltano/target-snowflake
1. Add a [webhook](https://docs.gitlab.com/ee/ci/triggers/#triggering-a-pipeline-from-a-webhook) to trigger the `meltano/meltano` pipeline.
1. Publish PyPI packages of these package (not for now)
1. We could mirror this repo on GitHub if we want (not for now)

### Discoverability

We maintain a curated list of taps/targets that are expected to work out of the box with Meltano. Meltano also helps the CLI user find components via a `discover` command.

Get a list of extractors:

```bash
meltano discover extract
tap-demo==...
tap-zendesk==1.3.0
tap-marketo==...
...
```

Or a list of loaders

```bash
$ meltano discover load
target-demo==...
target-snowflake==git+https://gitlab.com/meltano/target-snowflake@master.git
target-postgres==...
```

## DigitalOcean snapshot

To build a Droplet snapshot, one should use Packer.

```
docker run --rm -e DIGITALOCEAN_TOKEN=<PERSONAL_TOKEN> -v $(pwd)/cloud/packer:/packer -w /packer hashicorp/packer:latest build marketplace-image.json
```

The snapshot should be available under `meltano-<timestamp>` on DigitalOcean.

## Tmuxinator

Tmuxinator is a way for you to efficiently manage multiple services when starting up Meltano.

### Why Tmuxinator?

In order to run applications, you need to run multiple sessions and have to do a lot of repetitive tasks (like sourcing your virtual environments). So we have created a way for you to start and track everything in its appropriate panes with a single command.

1. Start up Docker
1. Start Meltano API
1. Start the web app

It's a game changer for development and it's worth the effort!

### Requirements

1. [tmux](https://github.com/tmux/tmux) - Recommended to install with brew
1. [tmuxinator](https://github.com/tmuxinator/tmuxinator)

This config uses `$MELTANO_VENV` to source the virtual environment from. Set it to the correct directory before running tmuxinator.

### Instructions

1. Make sure you know what directory your virtual environment is. It is normally `.venv` by default.
1. Run the following commands. Keep in mind that the `.venv` in line 2 refers to your virtual environment directory in Step #1.

```bash
cd path/to/meltano
MELTANO_VENV=.venv tmuxinator local
```

### Resources

- [Tmux Cheat Sheet & Quick Reference](https://tmuxcheatsheet.com/)
